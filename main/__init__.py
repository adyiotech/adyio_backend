from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask_moment import Moment
from flask_cors import CORS
from main.config import config
from celery import Celery

app = Flask(__name__, template_folder='../templates')
app.config.from_object(config)


# Register these in their own function so they don't pollute
# the main namespace
# Loading these here lets sqlalchemy know about the models
# and allows the controllers/errors to execute their hooks to
# create the routes
def _register_subpackages():
    import main.models
    import main.errors
    import main.controllers


# Initialize Celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

# Initializes the application with the following extensions.
mail = Mail()
mail.init_app(app)

moment = Moment()
moment.init_app(app)

db = SQLAlchemy()
db.init_app(app)
_register_subpackages()
db.create_all(app=app)

cor = CORS()
cor.init_app(app)
