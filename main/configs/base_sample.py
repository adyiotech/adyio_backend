import os


class Config(object):
    DEBUG = False

    # SQL Alchemy
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:Adu230999@localhost:3306/adyio_base'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Celery
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

    # JWT
    JWT_SECRET = 'base'
    JWT_LIFE_TIME = 60 * 60 * 24  # seconds
    JWT_ALGORITHMS = 'HS256'

    # Google mail
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
    MAIL_DEFAULT_SENDER = 'adyiotech@gmail.com'

    # Flask
    SECRET_KEY = os.environ.get('SECRET_KEY')
    MAX_CONTENT_LENGTH = 10 * 1024 * 1024  # Maximum content capacity is 10MB

    # Adyio
    ADYIO_MAIL_SUBJECT_PREFIX = 'Adyio'

    # NeverBounce
    NEVERBOUNCE_API_KEY = ''

    # Imgur
    IMGUR_CLIENT_ID = ''

    # Other
    MAXIMUM_CONFIRMATION_CODE_TRY = 5
    LEGAL_MIN_AGE = 13
    MAX_ACCOUNT_REGISTERED = 3
