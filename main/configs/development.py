from main.configs.base import Config


class _DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:Adu230999@localhost:3306/adyio_development'


config = _DevelopmentConfig
