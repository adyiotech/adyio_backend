from main.config import config
import requests

IMGUR_UPLOAD_IMAGE_URL = 'https://api.imgur.com/3/upload'
IMGUR_UNAUTHED_DELETE_IMAGE_URL = 'https://api.imgur.com/3/image/{}'


def upload_image(image):
    '''Return link and deletehash of an image in included in the response when requesting to upload an image to Imgur'''
    headers = {
        'Authorization': 'Client-ID {}'.format(config.IMGUR_CLIENT_ID)
    }
    try:
        api_response = requests.post(
            IMGUR_UPLOAD_IMAGE_URL,
            headers=headers,
            data={'image': image}
        ).content
        data = api_response['data']
        return data['link'], data['deletehash']
    except Exception:
        raise Exception('There is an error updating image to Imgur server')


def delete_image(imageDeleteHash):
    headers = {
        'Authorization': 'Client-ID {}'.format(config.IMGUR_CLIENT_ID)
    }
    try:
        api_response = requests.delete(
            IMGUR_UNAUTHED_DELETE_IMAGE_URL.format(imageDeleteHash),
            headers=headers
        ).content
    except Exception:
        raise Exception('There is an error deleting image from Imgur server')
