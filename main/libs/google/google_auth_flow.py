import google_auth_oauthlib.flow

from main.config import config

BASE_API_URL = 'https://openidconnect.googleapis.com/v1'


class GoogleAuthFlow:

    def __init__(self, client_secrets_file_path=None, scopes=None):
        try:
            self.flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
                client_secrets_file_path,
                scopes=scopes
            )
            self.flow.redirect_uri = config.GOOGLE_REDIRECT_URI
        except Exception:
            raise Exception('There is an error connecting with Google service')

    def fetch_token(self, authorization_code=None):
        try:
            self.flow.fetch_token(code=authorization_code)
        except Exception:
            raise Exception('There is an error getting access to Google service')

    def fetch_user_info(self):
        try:
            session = self.flow.authorized_session()
            google_api_response = session.get(BASE_API_URL + '/userinfo').json()
            return google_api_response
        except Exception:
            raise Exception('There is an error getting user information from Google service')
