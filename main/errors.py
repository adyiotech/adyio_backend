from flask import jsonify
from marshmallow import fields, Schema
from main import app


class Error(Exception):
    def __init__(self, error_data=None):
        super(Error)
        self.error_data = error_data or {}

    def to_response(self):
        resp = jsonify(ErrorSchema().dump(self))
        resp.status_code = self.status_code
        return resp


class ErrorSchema(Schema):
    error_code = fields.Int()
    error_message = fields.String()
    error_data = fields.Raw()


class StatusCode:
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    METHOD_NOT_ALLOWED = 405


class ErrorCode:
    BAD_REQUEST = 40000  #
    VALIDATION_ERROR = 40001  #
    USER_ALREADY_EXISTS = 40002  #
    USER_DOES_NOT_EXIST = 40003  #
    NOT_ENOUGH_CREDITS = 40006  #
    UNAUTHORIZED = 40100  #
    NOT_FOUND = 40400  #
    METHOD_NOT_ALLOWED = 40007  #
    SESSION_EXPIRED = 40014  # EP errors
    NOT_HAVE_ADMINISTRATOR_PERM = 40050  # Assign role with not administrator user
    NICK_NAME_EXISTED = 40053  #
    PHONE_NUMBER_EXISTED = 40055  #
    PHONE_NUMBER_NOT_VERIFIED = 40056  #
    SMS_CODE_NOT_MATCH = 40057  #
    INVALID_INPUT = 40060  #

    USER_ERROR = 40080  #

    OUT_OF_SERVICE = 40094  #

    PERMISSION_DENIED = 40301  #
    INCOMPLETE_ACCOUNT = 40302
    # Use when users try to signup with existed email
    USER_EMAIL_ALREADY_EXISTED = 40096  #
    # Use when email used to sign in is invalid
    INVALID_EMAIL = 40097
    # Use when user whose email is not confirmed trying to access protected resources
    UNCONFIRMED_EMAIL = 40098
    # Use for limiting creating trial account rate
    LIMIT_SENDING_REQUEST_REACHED = 40099  #


class UserEmailAlreadyExistedError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.USER_EMAIL_ALREADY_EXISTED
    error_message = 'This email is already registered. Click here to login.'


class InvalidEmail(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.INVALID_EMAIL
    error_message = 'This email is invalid. Please try another one.'


class UnconfirmedEmail(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.UNCONFIRMED_EMAIL
    error_message = 'This email has not been confirmed. Please use confirmation code to confirm this email.'


class LimitSendingRequestReached(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.LIMIT_SENDING_REQUEST_REACHED
    error_message = 'Exceeded the request limit. Please try again later.'


class BadRequest(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = "Bad request"

    def __init__(self, error_data=None, code=ErrorCode.BAD_REQUEST, message='Bad Request'):
        super(self.__class__, self).__init__(error_data)
        self.error_code = code
        self.error_message = message


class Unauthorized(Error):
    status_code = StatusCode.UNAUTHORIZED
    error_code = ErrorCode.UNAUTHORIZED
    error_message = 'Unauthorized'


class InvalidInput(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.INVALID_INPUT
    error_message = 'Invalid input'


class NotFound(Error):
    status_code = StatusCode.NOT_FOUND
    error_code = ErrorCode.NOT_FOUND
    error_message = 'Not found'

    def __init__(self, code=ErrorCode.NOT_FOUND, message='Not found', data=None):
        super(self.__class__, self).__init__(data)
        self.error_code = code
        self.error_message = message


class MethodNotAllowed(Error):
    status_code = StatusCode.METHOD_NOT_ALLOWED
    error_code = ErrorCode.METHOD_NOT_ALLOWED
    error_message = 'Method not allowed'


class ValidationError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.VALIDATION_ERROR
    error_message = 'Validation error'

    def __init__(self, code=ErrorCode.VALIDATION_ERROR, message='Validation error', data=None):
        super(self.__class__, self).__init__(data)
        self.error_code = code
        self.error_message = message


class UserAlreadyExists(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.USER_ALREADY_EXISTS
    error_message = 'User already exists'


class UserDoesNotExist(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.USER_DOES_NOT_EXIST
    error_message = 'User does not exist'


class UserNotConfirmed(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = "You haven't confirmed your account yet."


class NotEnoughCredits(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.NOT_ENOUGH_CREDITS
    error_message = 'You do not have enough credits'


# When user inputs the existed phone number that belong to another user
class PhoneNumberUsed(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.PHONE_NUMBER_EXISTED
    error_message = 'Phone number is used by another user'


class PhoneNumberNotVerified(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.PHONE_NUMBER_NOT_VERIFIED
    error_message = 'Phone number not verified'


class SmsCodeNotMatch(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.SMS_CODE_NOT_MATCH
    error_message = 'Sms code does not match'


class InvalidConfirmationCode(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = "Incorrect or expired confirmation code."


class PermissionDenied(Error):
    status_code = StatusCode.FORBIDDEN
    error_code = ErrorCode.PERMISSION_DENIED
    error_message = 'Permission Denied'


class UserError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.USER_ERROR
    error_message = 'User fault'

    def __init__(self, code=ErrorCode.USER_ERROR, message='User fault', data=None):
        super(self.__class__, self).__init__(data)
        self.error_code = code
        self.error_message = message


class UserErrors:
    NICKNAME_LENGTH_LIMIT = 'Nickname must be at least 6 characters in length.'
    UNAVAILABLE_NICKNAME = 'Nickname is not available. Please choose another one.'
    # This message is used for Explainer, Auditor
    PASSWORD_LENGTH_LIMIT = 'Password must be at least 8 characters in length.'
    # This message is used for Asker
    ASKER_PASSWORD_LENGTH_LIMIT = 'Password must be at least 6 characters in length.'
    PASSWORD_WHITESPACES = 'Password cannot contain whitespaces.'
    INVALID_CURRENT_PASSWORD = 'Invalid current password.'
    PASSWORD_CONTAIN_SPECIAL_CHARACTERS = 'Password must not contain special characters.'
    ACCOUNT_PROBLEM = 'Your account has a problem. Please contact with our supporters for more information.'
    CONFIRM_PASSWORD_DOES_NOT_MATCH = 'Confirm password does not match.'
    EXISTED_ACCOUNT = 'This account has already existed. Please log in instead.'
    NON_EXISTENT_ACCOUNT = 'This account does not exist.'
    EMPTY_FULL_NAME = 'Please enter a full name.'
    EMPTY_EMAIL = 'Please enter a valid email address.'
    EMPTY_PASSWORD = 'Please enter a password.'
    FULL_NAME_LENGTH_LIMIT = 'Name must be at least 6 characters in length.'
    INVALID_PHONE_NUMBER = 'Please enter a valid phone number.'


class EmailAndPasswordNotMatch(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = "Invalid email or password."


class OutOfService(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.OUT_OF_SERVICE
    error_message = "Sorry our service is not available at the moment. Please try again later."


class NicknameLengthError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.NICK_NAME_EXISTED
    error_message = 'Nickname must be at least 6 characters in length.'


class NotAllowedConnectionError(Error):
    status_code = StatusCode.FORBIDDEN
    error_code = ErrorCode.USER_ERROR
    error_message = 'You are not allowed to connect now'


class PrivacyError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = 'Privacy policy required more permissions'


class TermOfServiceError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = 'Accepted term of service is required'

    def __init__(self, code=error_code, message=error_message, data=None):
        super(self.__class__, self).__init__(data)
        self.error_code = code
        self.error_message = message


class DatabaseError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = ErrorCode.BAD_REQUEST
    error_message = 'Unexpected error with database operation.'


BraintreeErrors = {
    2001: 'Insufficient Funds. At the time of the transaction, '
          'the account did not have sufficient funds to cover the transaction amount. '
          'Please check your method of payment and try again.',
    2004: 'Expired Card',
    2005: 'Invalid Credit Card Number',
    2006: 'Invalid Expiration Date',
    2007: 'No Account',
    2008: 'Card Account Length Error',
    2009: 'No Such Issuer',
    2010: 'Card Issuer Declined CVV',
    2011: 'We were unable to complete this transaction. '
          'Please try a different form of payment and try again.  '
          'If you have any questions, contact our Support team for more details. sheets.support@gotitapp.co',
    2016: 'Duplicate Transaction',
    2017: 'Cardholder Stopped Billing',
    2018: 'Cardholder Stopped All Billing',
    2024: 'Card Type Not Enabled',
    2025: None,
    2026: None,
    2027: None,
    2028: None,
    2029: None,
    2030: None,
    2039: 'Invalid Authorization Code',
    2040: None,
    2041: 'Declined - Call For Approval',
    2042: None,
    2045: None,
    2048: None,
    2049: None,
    2051: 'Credit Card Number does not match the method of payment',
    2054: None,
    2055: None,
    2056: None,
    2059: 'Address Verification Failed. PayPal was unable to verify that the transaction qualifies for'
          ' Seller Protection because the address was improperly formatted.',
    2062: None,
    2063: 'PayPal Business Account preference resulted in the transaction failing. '
          'You will receive this decline code if you have elected to block certain payment types, '
          'such as eChecks or foreign currencies.',
    2064: 'Invalid Currency Code',
    2065: "Refund Time Limit Exceeded. "
          "By default, PayPal requires that refunds are issued within 60 days of the sale, "
          "but this can be adjusted by contacting PayPal's Support team.",
    2066: "PayPal Business Account Restricted. "
          "You'll need to contact PayPal's Support team to resolve this issue with your account. "
          "Then, you can attempt the transaction again.",
    2067: 'Authorization Expired. '
          'This indicates that the PayPal authorization is no longer valid. '
          'PayPal cannot ensure that 100% of the funds will be available after a 3-day honor period.',
    2068: "PayPal Business Account Locked or Closed. "
          "You'll need to contact PayPal's Support team to resolve an issue with your account. "
          "Once resolved, you can attempt to process the transaction again.",
    2069: 'PayPal Blocking Duplicate Order IDs',
    2070: 'PayPal Buyer Revoked Future Payment Authorization',
    2071: 'PayPal Payee Account Invalid Or Does Not Have a Confirmed Email',
    2072: 'PayPal Payee Email Incorrectly Formatted',
    2073: None,
    2074: "Funding Instrument In The PayPal Account Was Declined By The Processor Or Bank, "
          "Or It Can't Be Used For This Payment.",
    2075: 'Payer Account Is Locked Or Closed',
    2076: 'Payer Cannot Pay For This Transaction With PayPal. '
          'You may receive this response if you create transactions '
          'using the email address registered with your PayPal Business account.',
    2077: None,
    2078: None,
    2079: "PayPal Merchant Account Configuration Error. "
          "You'll need to contact Braintree's Support team to resolve"
          " an issue with your account. Once resolved, you can attempt to process the transaction again.",
    2080: None,
    2081: 'PayPal pending payments are not supported'
}


class BraintreeError(Error):
    status_code = StatusCode.BAD_REQUEST
    error_code = 2000
    error_message = 'We were unable to complete this transaction, please contact your bank for more details.'

    def __init__(self, braintree_error_code=None):
        super(self.__class__, self).__init__()
        if braintree_error_code is None:
            return None
        if braintree_error_code not in BraintreeErrors.keys():
            return None
        self.error_code = braintree_error_code
        error_message = BraintreeErrors.get(braintree_error_code)
        if error_message is None:
            self.error_message = 'We were unable to complete this transaction,' \
                                 ' please contact sheets.support@gotitapp.co for more details.'
        else:
            self.error_message = error_message


@app.errorhandler(404)
def page_not_found(error):
    return NotFound().to_response()


@app.errorhandler(405)
def method_not_allowed(error):
    return MethodNotAllowed().to_response()


@app.errorhandler(Exception)
def error_handler(error):
    if isinstance(error, Error):
        return error.to_response()

    return jsonify(error_message=str(error)), 500
