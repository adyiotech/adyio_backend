from functools import wraps
from main.models.user import UserModel
from flask import request
from main import errors, db
from main.config import config
from main.models.account_confirmation import AccountConfirmationModel
from main.errors import ValidationError
from main.engines.email_sending import send_email_via_gmail_smtp
from werkzeug.security import safe_str_cmp
import jwt
import random


def parse_args_with(schema):
    def parse_args_with_decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            request_args = request.get_json() or {}
            if request.method == 'GET':
                request_args = request.args.to_dict()
            if 'file' in request.files:
                request_args['file'] = request.files['file']
            parsed_args = schema.load(request_args)
            kwargs['args'] = parsed_args
            return f(*args, **kwargs)

        return decorated_function

    return parse_args_with_decorator


def jwt_required(audience):
    def validate_jwt(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            authorization_header = request.headers.get('Authorization')
            if authorization_header is None:
                raise errors.Unauthorized
            token_parts = authorization_header.split(' ')
            if len(token_parts) != 2:
                raise errors.Unauthorized
            token = token_parts[1]
            try:
                payload = jwt.decode(token, config.JWT_SECRET, algorithms=config.JWT_ALGORITHMS, audience=audience)
            except jwt.InvalidTokenError:
                raise errors.Unauthorized
            for field in ['email', 'access_token_nonce']:
                if field not in payload:
                    raise errors.Unauthorized
            email = payload.get('email')
            user = UserModel.query.filter_by(email=email).one_or_none()
            if not user or not user.confirmed:
                raise errors.Unauthorized
            if not safe_str_cmp(user.access_token_nonce, payload.get('access_token_nonce')):
                raise errors.Unauthorized
            kwargs['user'] = user

            return f(*args, **kwargs)

        return decorated_function

    return validate_jwt


def create_user_confirmation_code_with_confirmation_email(nickname, email):
    # Generate confirmation code
    confirmation_code = random.randrange(100000, 999999)
    confirmation = AccountConfirmationModel.query.filter_by(email=email).one_or_none()
    # Ensure that there is only one confirmation code for one user
    if confirmation:
        confirmation.confirmation_code = confirmation_code
    else:
        confirmation = AccountConfirmationModel(email=email, confirmation_code=confirmation_code)
    db.session.add(confirmation)
    try:
        db.session.commit()
    except ValidationError:
        raise errors.ValidationError(confirmation.validation_errors)

    send_email_via_gmail_smtp.delay(
        from_email=config.MAIL_USERNAME,
        to_email=email,
        subject='Confirm your account',
        template='account_confirmation.html',
        nickname=nickname,
        confirmation_code=confirmation_code,
    )


# Calculate the time difference between 2 datetime object
def time_difference_in_second(before, after):
    time_diff = after - before
    return time_diff.seconds
