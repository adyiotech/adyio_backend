from main import app
from main.core import jwt_required


@app.route('/users/notifications', methods=['GET'])
@jwt_required('user')
def get_user_notifications(**kwargs):
    pass
