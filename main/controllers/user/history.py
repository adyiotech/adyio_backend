from main import app
from main.core import jwt_required


@app.route('/users/histories/matching_histories', methods=['GET'])
@jwt_required('user')
def get_user_matching_history(**kwargs):
    pass


@app.route('/users/histories/balance_histories', methods=['GET'])
@jwt_required('user')
def get_user_balance_history(**kwargs):
    pass
