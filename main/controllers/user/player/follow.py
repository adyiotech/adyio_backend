from main import app
from main.core import jwt_required


@app.route('/players/followers', methods=['GET'])
@jwt_required('user')
def get_player_followers():
    pass
