from main import app
from main.core import jwt_required


@app.route('/players/settings/information', methods=['POST'])
@jwt_required('user')
def update_player_information():
    pass


@app.route('/players/settings/information', methods=['GET'])
@jwt_required('user')
def get_player_information():
    pass
