from main import app
from main.core import jwt_required


@app.route('/players/histories/matching_histories', methods=['GET'])
@jwt_required('user')
def get_player_matching_history():
    pass
