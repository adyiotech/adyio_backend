from main import app
from main.core import jwt_required


@app.route('/users/followees', methods=['GET'])
@jwt_required('user')
def get_user_followees(**kwargs):
    pass


@app.route('/users/followees', methods=['POST'])
@jwt_required('user')
def create_user_followee(**kwargs):
    pass


@app.route('/users/followees/<int:followee_id>', methods=['DELETE'])
@jwt_required('user')
def delete_user_followee(**kwargs):
    pass
