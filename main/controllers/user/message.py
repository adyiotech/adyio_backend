from main import app
from main.core import jwt_required


@app.route('/users/messages/<string:message_id>', methods=['POST'])
@jwt_required('user')
def create_message(**kwargs):
    pass


@app.route('/users/messages/<string:message_id>', methods=['GET'])
@jwt_required('user')
def get_messages(**kwargs):
    pass
