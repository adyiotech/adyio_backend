from flask import jsonify, request
from savalidation import ValidationError
from main.core import parse_args_with
from main.schemas.user import (
    UserAuthSchema,
    UserRegisterInformationSchema,
    UserRegisterConfirmationSchema
)
from werkzeug.security import generate_password_hash, check_password_hash
from main import errors, db
from main.models.user import UserModel
from main.models.account_confirmation import AccountConfirmationModel
from main.models.ip import IpModel
from main import app, jwttoken
from main.core import jwt_required, time_difference_in_second
from main.jwttoken import generate_access_token_nonce
from main.core import create_user_confirmation_code_with_confirmation_email
from main.config import config
import datetime
import logging


@app.route('/users/auth', methods=['POST'])
@parse_args_with(UserAuthSchema())
def login_user_normal(args):
    email = args.get('email')
    password = args.get('password')

    user = UserModel.query.filter_by(email=email).one_or_none()
    if not user:
        raise errors.UserDoesNotExist()
    if not user.confirmed:
        raise errors.UserNotConfirmed()
    if not check_password_hash(password=password, pwhash=user.hashed_password):
        raise errors.EmailAndPasswordNotMatch()

    return jsonify(access_token=jwttoken.encode(user))


@app.route('/users', methods=['POST'])
@parse_args_with(UserRegisterInformationSchema())
def register_user_information(args):
    # Find IP address and prevent mass registering
    ip_address = request.remote_addr
    ip = IpModel.query.filter_by(address=ip_address).one_or_none()
    if ip:
        if ip.account_registered == config.MAX_ACCOUNT_REGISTERED:
            if time_difference_in_second(before=ip.updated, after=datetime.datetime.now()) < 60 * 60 * 24:
                raise errors.LimitSendingRequestReached
            else:
                ip.account_registered = 0
                db.session.add(ip)
                db.session.commit()

    email = args.get('email')
    if UserModel.query.filter_by(email=email).one_or_none():
        raise errors.UserEmailAlreadyExistedError
    password = args.get('password')
    hashed_password = generate_password_hash(password)
    # Create new user
    user = UserModel(
        nickname=args.get('nickname'),
        email=email,
        hashed_password=hashed_password,
        date_of_birth=args.get('date_of_birth'),
        gender=args.get('gender'),
        access_token_nonce=generate_access_token_nonce()
    )

    db.session.add(user)
    try:
        db.session.commit()
    except ValidationError:
        raise errors.ValidationError(user.validation_errors)

    create_user_confirmation_code_with_confirmation_email(nickname=args.get('nickname'), email=email)
    if not ip:
        ip = IpModel(address=ip_address, account_registered=1)
    else:
        ip.account_registered += 1
    db.session.add(ip)
    db.session.commit()
    return jsonify(message='Account created successfully. Account confirmation email sent.'), 201


@app.route('/users/confirmation_codes', methods=['POST'])
@jwt_required('user')
def create_user_confirmation_code(**kwargs):
    user = kwargs['user']
    create_user_confirmation_code_with_confirmation_email(user.nickname, user.email)


@app.route('/users/confirmations', methods=['POST'])
@parse_args_with(UserRegisterConfirmationSchema())
def register_user_confirmation(args):
    email = args.get('email')
    user = UserModel.query.filter_by(email=email).one_or_none()
    if user is None:
        raise errors.UserDoesNotExist
    if user.confirmed:
        return jsonify(message='Email has already confirmed.')
    confirmation = AccountConfirmationModel.query.filter_by(email=email).one_or_none()
    if confirmation is None:
        raise errors.InvalidConfirmationCode
    current_time = datetime.datetime.utcnow()
    time_diff = time_difference_in_second(confirmation.created, current_time)
    if time_diff > 60 * 30:  # 30 minutes
        db.session.delete(confirmation)
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            logging.exception(e)
            raise errors.DatabaseError
        raise errors.InvalidConfirmationCode

    if confirmation.failed_try == config.MAXIMUM_CONFIRMATION_CODE_TRY:
        time_diff = time_difference_in_second(confirmation.last_try_at, current_time)
        if time_diff > 60 * 60:  # 1 hour
            confirmation.failed_try = 0
            db.session.add(confirmation)
        else:
            raise errors.LimitSendingRequestReached()

    confirmation_code = args.get('confirmation_code')
    if confirmation.confirmation_code != confirmation_code:
        confirmation.failed_try += 1
        db.session.add(confirmation)
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            logging.exception(str(e))
            raise errors.DatabaseError
        raise errors.InvalidConfirmationCode
    user.confirmed = True

    db.session.add(user)
    db.session.delete(confirmation)
    try:
        db.session.commit()
    except ValidationError:
        raise errors.ValidationError(user.validation_errors)

    return jsonify(message='Account confirmed successfully.')


@app.route('/users/logout', methods=['POST'])
@jwt_required('user')
def logout_user(**kwargs):
    user = kwargs['user']
    user.access_token_nonce = generate_access_token_nonce()

    db.session.add(user)
    try:
        db.session.commit()
    except ValidationError:
        raise errors.ValidationError(user.validation_errors)

    return jsonify(message='Logged out successfully.')
