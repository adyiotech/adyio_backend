from main import app, errors, db
from main.core import jwt_required, parse_args_with, create_user_confirmation_code_with_confirmation_email
from main.schemas.base import FieldsQuerySchema
from main.models.user_game import UserGameModel
from main.models.game import GameModel
from main.models.user import UserModel
from main.models.imgur_image import ImgurImageModel
from main.engines.image_operating import upload_and_get_image_info_from_imgur, delete_image_from_imgur
from main.schemas.user_update_info import UserUpdateInfoSchema
from flask import jsonify
from werkzeug.security import check_password_hash, generate_password_hash
from savalidation import ValidationError


@app.route('/users/<string:nickname>/information', methods=['GET'])
@jwt_required('user')
@parse_args_with(FieldsQuerySchema())
def get_user_info(args, **kwargs):
    user = kwargs['user']
    _fields = args['fields']

    data = {}

    if 'nickname' in _fields:
        data['nickname'] = user.nickname

    if 'date_of_birth' in _fields:
        data['date_of_birth'] = user.date_of_birth

    if 'gender' in _fields:
        data['gender'] = user.gender

    if 'avatar_url' in _fields:
        data['avatar_url'] = user.avatar_url

    if 'short_biography' in _fields:
        data['short_biography'] = user.short_biography

    if 'long_biography' in _fields:
        data['long_biography'] = user.long_biography

    if 'games' in _fields:
        games = GameModel.query.join(UserGameModel, UserGameModel.game_id == GameModel.id) \
            .filter(UserGameModel.user_id == user.id)
        data['games'] = [game.name for game in games]

    if 'email' in _fields:
        data['email'] = user.email

    # Todo: Matching history with pagination
    if 'matching_history' in _fields:

        pass

    # Todo: Balance history with pagination
    if 'balance_history' in _fields:
        pass

    return jsonify(data)


@app.route('/users/<string:nickname>/information', methods=['PUT'])
@jwt_required('user')
@parse_args_with(UserUpdateInfoSchema())
def update_user_info(user, args, **kwargs):
    editable_fields = [
        'nickname', 'email', 'gender', 'date_of_birth', 'avatar_url',
        'short_biography', 'long_biography', 'password', 'file'
    ]
    if all(args[field] is None for field in editable_fields):
        raise errors.BadRequest(message='Nothing to update')

    if args['nickname']:
        user = UserModel.query.filter_by(nickname=args['nickname']).one_or_none()
        if user:
            raise errors.BadRequest('This nickname has been taken by another user. Please choose another one.')
        user.nickname = args['nickname']

    if args['email']:
        user = UserModel.query.filter_by(email=args['email']).one_or_none()
        if user:
            raise errors.UserEmailAlreadyExistedError
        user.email = args['email']

    if args['gender']:
        user.gender = args['gender']

    if args['date_of_birth']:
        user.date_of_birth = args['date_of_birth']

    # file in here is avatar image
    if args['file']:
        image_link, image_deletehash = upload_and_get_image_info_from_imgur(args['file'])
        image = ImgurImageModel.query.filter_by(url=image_link).one_or_none()
        if image:
            image.url = image_link
            image.deletehash = image_deletehash
        else:
            image = ImgurImageModel(url=image_link, deletehash=image_deletehash)
        db.session.add(image)
        try:
            db.session.commit()
        except Exception:
            raise errors.DatabaseError
        user.avatar_url = image_link

    if args['short_biography']:
        user.short_biography = args['short_biography']

    if args['long_biography']:
        user.long_biography = args['long_biography']

    if args['new_password']:
        if not args['old_password']:
            raise errors.BadRequest()
        if not check_password_hash(pwhash=user.hashed_password, password=args['old_password']):
            raise errors.EmailAndPasswordNotMatch
        user.hashed_password = generate_password_hash(args['new_password'])

    db.session.add(user)
    try:
        db.session.commit()
    except ValidationError:
        raise errors.ValidationError(user.validation_errors)

    return jsonify(message='Updated user information successfully')
