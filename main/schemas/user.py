from marshmallow import fields, validate
from main.schemas.base import BaseSchema


class UserAuthSchema(BaseSchema):
    email = fields.Email(required=True)
    password = fields.String(required=True, validate=[validate.Length(min=6)])


class GoogleAuthSchema(BaseSchema):
    authorization_code = fields.String(required=True)


class UserRegisterInformationSchema(BaseSchema):
    nickname = fields.String(required=True, validate=[validate.Length(min=1, max=64)])
    email = fields.Email(required=True)
    password = fields.String(required=True, validate=[validate.Length(min=6)])
    date_of_birth = fields.Date(required=True)
    gender = fields.String(required=True, validate=[validate.Length(min=1, max=16)])


class UserRegisterConfirmationSchema(BaseSchema):
    confirmation_code = fields.Integer(required=True)
    email = fields.Email(required=True)
