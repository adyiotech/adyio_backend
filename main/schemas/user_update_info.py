from main.schemas.base import BaseSchema
from marshmallow import fields, validates, validate
from datetime import datetime
from main import errors
from main.config import config


class UserUpdateInfoSchema(BaseSchema):
    email = fields.Email(missing=None)
    nickname = fields.String(missing=None, validate=[validate.Length(min=1, max=64)])
    old_password = fields.String(missing=None, validate=[validate.Length(min=6)])
    new_password = fields.String(missing=None, validate=[validate.Length(min=6)])
    date_of_birth = fields.Date(missing=None)
    gender = fields.String(missing=None)

    short_biography = fields.String(missing=None)
    long_biography = fields.Url(missing=None)

    file = fields.Raw(missing=None)

    @validates('date_of_birth')
    def validate_date_of_birth(self, date_of_birth):
        current_year = datetime.now().year
        if current_year - (config.LEGAL_MIN_AGE + 1) < date_of_birth.year:
            raise errors.BadRequest('Must be over 13 years old.')
