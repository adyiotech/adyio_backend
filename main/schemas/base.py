from marshmallow import Schema, EXCLUDE, pre_load, validates, fields, post_load
from main.libs.neverbounce.email_verification import validate_email


class BaseSchema(Schema):
    @pre_load()
    def strip_data(self, data, **kwargs):
        for key in data:
            if isinstance(data[key], str):
                data[key] = data[key].strip()
        return data

    @validates('email')
    def validate_email(self, email):
        if email:
            validate_email(email)

    class Meta:
        unknown = EXCLUDE


class FieldsQuerySchema(BaseSchema):
    fields = fields.String(required=False)

    @post_load
    def parse_field(self, data):
        _fields = data.get('fields', None)
        if _fields:
            _fields = set(_fields.split(','))
            data['fields'] = _fields
        else:
            data['fields'] = set()

        return data
