from main.models.base import TimestampMixin
from main import db


class PlayerReviewModel(db.Model, TimestampMixin):
    __tablename__ = 'player_review'

    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('UserModel')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    player = db.relationship('PlayerModel')
    player_id = db.Column(db.Integer, db.ForeignKey('player.id'), nullable=False)
    content = db.Column(db.Text, nullable=False)

    liked = db.Column(db.Integer, default=0)

    def __init__(self, *args, **kwargs):
        super(PlayerReviewModel, self).__init__(*args, **kwargs)
