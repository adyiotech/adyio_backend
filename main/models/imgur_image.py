from main.models.base import TimestampMixin
from main import db


class ImgurImageModel(db.Model, TimestampMixin):
    __tablename__ = 'imgur_image'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(64), nullable=False, unique=True)
    deletehash = db.Column(db.String(64), nullable=False)

    def __init__(self, *args, **kwargs):
        super(ImgurImageModel, self).__init__(*args, **kwargs)
