from main.models.base import TimestampMixin
from savalidation import ValidationMixin
from main import db
from datetime import datetime


class UserModel(db.Model, TimestampMixin, ValidationMixin):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), nullable=False, unique=True)
    email = db.Column(db.String(64), nullable=False, unique=True)
    hashed_password = db.Column(db.String(256), nullable=False)
    date_of_birth = db.Column(db.Date, nullable=True)
    gender = db.Column(db.String(16), nullable=False)
    avatar_url = db.Column(db.String(256), nullable=True)
    status = db.Column(db.Boolean, nullable=False, default=False)

    type = db.Column(db.String(16), nullable=False, default='user')
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    access_token_nonce = db.Column(db.String(16), nullable=False)

    last_request_at = db.Column(db.DateTime, onupdate=datetime.utcnow, nullable=True)

    short_biography = db.Column(db.Text, nullable=True)
    long_biography = db.Column(db.Text, nullable=True)

    followees = db.relationship(
        'FollowModel',
        foreign_keys='[FollowModel.user_id]',
        backref=db.backref('follower', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )

    games = db.relationship(
        'UserGameModel',
        foreign_keys='[UserGameModel.user_id]',
        backref=db.backref('user', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )

    def __init__(self, *args, **kwargs):
        super(UserModel, self).__init__(*args, **kwargs)
