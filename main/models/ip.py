from main.models.base import TimestampMixin
from main import db


class IpModel(db.Model, TimestampMixin):
    __tablename__ = 'ip'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(16), nullable=False, unique=True)
    account_registered = db.Column(db.Integer, default=0)

    def __init__(self, *args, **kwargs):
        super(IpModel, self).__init__(*args, **kwargs)
