from main.models.base import TimestampMixin
from savalidation import ValidationMixin
from main import db
from datetime import datetime


class AccountConfirmationModel(db.Model, TimestampMixin, ValidationMixin):
    __tablename__ = 'account_confirmation'

    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('UserModel')
    email = db.Column(db.String(64), db.ForeignKey('user.email'), nullable=False)
    confirmation_code = db.Column(db.Integer, nullable=False)
    last_try_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)
    failed_try = db.Column(db.Integer, nullable=False, default=0)

    def __init__(self, *args, **kwargs):
        super(AccountConfirmationModel, self).__init__(*args, **kwargs)
