from savalidation import ValidationMixin
from main import db


class GameModel(db.Model, ValidationMixin):
    __tablename__ = 'game'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    genre = db.relationship('GenreModel')
    genre_id = db.Column(db.Integer, db.ForeignKey('genre.id'), nullable=False)

    def __init__(self, *args, **kwargs):
        super(GameModel, self).__init__(*args, **kwargs)
