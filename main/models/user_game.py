from savalidation import ValidationMixin
from main import db


class UserGameModel(db.Model, ValidationMixin):
    __tablename__ = 'user_game'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    game = db.relationship('GameModel')
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), nullable=False)

    def __init__(self, *args, **kwargs):
        super(UserGameModel, self).__init__(*args, **kwargs)
