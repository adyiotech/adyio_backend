from main.models.base import TimestampMixin
from main import db


class PlayerModel(db.Model, TimestampMixin):
    __tablename__ = 'player'

    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('UserModel', cascade='all, delete-orphan', single_parent=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    amount = db.Column(db.Integer, nullable=False)
    max_hour_per_request = db.Column(db.Integer, nullable=False)

    followers = db.relationship(
        'FollowModel',
        foreign_keys='[FollowModel.player_id]',
        backref=db.backref('followee', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )

    def __init__(self, *args, **kwargs):
        super(PlayerModel, self).__init__(*args, **kwargs)
