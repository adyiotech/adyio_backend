from savalidation import ValidationMixin
from main import db


class GenreModel(db.Model, ValidationMixin):
    __tablename__ = 'genre'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)

    def __init__(self, *args, **kwargs):
        super(GenreModel, self).__init__(*args, **kwargs)
