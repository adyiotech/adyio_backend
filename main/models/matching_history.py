from main.models.base import TimestampMixin
from main import db


class MatchingHistoryModel(db.Model, TimestampMixin):
    __tablename__ = 'matching_history'

    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('UserModel')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    player = db.relationship('PlayerModel')
    player_id = db.Column(db.Integer, db.ForeignKey('player.id'), nullable=False)

    # Todo: When a match begin, fix status into 'ongoing'
    status = db.Column(db.String(16), nullable=False)

    hour = db.Column(db.Integer)
    amount = db.Column(db.Integer)

    def __init__(self, *args, **kwargs):
        super(MatchingHistoryModel, self).__init__(*args, **kwargs)
