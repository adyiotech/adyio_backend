from main.models.base import TimestampMixin
from savalidation import ValidationMixin
from main import db


class FollowModel(db.Model, TimestampMixin, ValidationMixin):
    __tablename__ = 'follow'

    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('UserModel')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    player = db.relationship('PlayerModel')
    player_id = db.Column(db.Integer, db.ForeignKey('player.id'), nullable=False)

    def __init__(self, *args, **kwargs):
        super(FollowModel, self).__init__(*args, **kwargs)

