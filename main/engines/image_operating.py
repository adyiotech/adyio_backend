from main import celery
from main import errors
from main.libs.imgur.image_operating import upload_image, delete_image

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'pdf'}


def allowed_image_extension(image_name):
    return '.' in image_name and \
           image_name.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@celery.task
def upload_and_get_image_info_from_imgur(image):
    if allowed_image_extension(image.filename):
        image_link, image_deletehash = upload_image(image)
        return image_link, image_deletehash
    else:
        raise errors.BadRequest(
            'File extensions must be one of the following: {}'.format(', '.join(ALLOWED_EXTENSIONS))
        )


@celery.task
def delete_image_from_imgur(imageDeleteHash):
    if imageDeleteHash:
        delete_image(imageDeleteHash)
