from flask import render_template
from flask_mail import Message
from main import app, celery, mail
from main.config import config
import os

GMAIL_MAX_NON_ASYNC_RECIPIENTS = 500
EMAIL_TEMPLATE_DIR = 'email'


@celery.task
def send_email_via_gmail_smtp(from_email, to_email, subject, template, **template_args):
    """Background task to send an email with Flask-Mail"""
    with app.app_context():
        msg = Message(
            config.ADYIO_MAIL_SUBJECT_PREFIX + ' ' + subject,
            sender=from_email,
            recipients=[to_email]
        )
        template_path = os.path.join(EMAIL_TEMPLATE_DIR, template)
        msg.html = render_template(template_path, **template_args)
        mail.send(msg)


def send_emails_via_gmail_smtp(to, subject, template, **template_args):
    pass
